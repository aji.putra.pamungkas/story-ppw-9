from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from . import models

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1','password2']
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Username..',
        'class' : 'inputtype'
	}
    input_attrs2 = {
		'type' : 'text',
		'placeholder' : 'Email..',
        'class' : 'inputtype'
	}
    input_attrs3 = {
		'type' : 'text',
		'placeholder' : 'Password..',
        'class' : 'inputtype'
	}
    input_attrs4 = {
		'type' : 'text',
		'placeholder' : 'Verify your Password..',
        'class' : 'inputtype'
	}
    
    username = forms.CharField(label='', max_length=50, widget=forms.TextInput(attrs=input_attrs1))
    email = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs2))
    password1 = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs3))
    password2 = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs4))

class addPostForm(forms.ModelForm):
    class Meta:
        model = models.Messages
        fields = ['post']
    input_attrs = {
        'rows' : 7,
        'placeholder' : 'Post your thought..',
        'class' : 'inputtype',
    }
    post = forms.CharField(label = '', max_length = 99999, widget=forms.Textarea(attrs=input_attrs))

    
