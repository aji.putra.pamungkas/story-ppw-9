from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, feeds, myFeeds, addPost, savePost, loginPage, logout, signUpPage
from .models import Messages

"""
@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    Base class for functional test cases with selenium.

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
"""
class mainTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_home_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)


    def test_feeds_url_is_exist(self):
        response = Client().get('/feeds/')
        self.assertEqual(response.status_code, 200)
    
    def test_feeds_using_template(self):
        response = Client().get('/feeds/')
        self.assertTemplateUsed(response, 'main/feedsPage.html')

    def test_feeds_using_func(self):
        found = resolve('/feeds/')
        self.assertEqual(found.func, feeds)


    def test_myfeeds_url_is_exist(self):
        response = Client().get('/my-feeds/')
        self.assertEqual(response.status_code, 200)
    
    def test_myfeeds_using_template(self):
        response = Client().get('/my-feeds/')
        self.assertTemplateUsed(response, 'main/myFeedsPage.html')

    def test_myfeeds_using_func(self):
        found = resolve('/my-feeds/')
        self.assertEqual(found.func, myFeeds)


    def test_login_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_page_using_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'main/loginPage.html')

    def test_login_using_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginPage)

    
    def test_signup_page_url_is_exist(self):
        response = Client().get('/sign-up/')
        self.assertEqual(response.status_code, 200)
    
    def test_signup_page_using_template(self):
        response = Client().get('/sign-up/')
        self.assertTemplateUsed(response, 'main/signUpPage.html')

    def test_signup_using_func(self):
        found = resolve('/sign-up/')
        self.assertEqual(found.func, signUpPage)

    
    def test_add_post_url_is_exist(self):
        response = Client().get('/add-post/')
        self.assertEqual(response.status_code, 200)

    def test_add_post_using_template(self):
        response = Client().get('/add-post/')
        self.assertTemplateUsed(response, 'main/addPost.html')

    def test_add_post_using_func(self):
        found = resolve('/add-post/')
        self.assertEqual(found.func, addPost)

    def test_save_post_using_func(self):
        found = resolve('/save-post/')
        self.assertEqual(found.func, savePost)
    
    def test_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_model_can_create_new_activity(self):
        new_feeds = Messages.objects.create(username = "a", message = "b")
        new_feeds.save()
        count_all = Messages.objects.all().count()
        self.assertEqual(count_all, 1)

    def test_can_save_POST_request(self):
        response = self.client.post('/save-post/', data={'post': 'a'})
        count_all = Messages.objects.all().count()
        self.assertEqual(count_all, 1)

        self.assertEqual(response.status_code,302)

    

