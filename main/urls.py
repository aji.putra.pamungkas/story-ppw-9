from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('feeds/', views.feeds, name='feeds'),
    path('my-feeds/', views.myFeeds, name='myFeeds'),
    path('add-post/', views.addPost, name = 'addPost'),
    path('save-post/',views.savePost, name = 'savePost'),
    path('login/', views.loginPage, name = 'loginPage'),
    path('sign-up/', views.signUpPage, name = 'signUpPage'),
    path('logout/', views.logoutPage, name = 'logouts'),
]
