from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import HttpResponseRedirect

from.forms import CreateUserForm, addPostForm
from .models import Messages

def home(request):
    return render(request, 'main/home.html')

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('/feeds/')
    else :
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username = username, password = password)

            if user is not None :
                login(request, user)
                return redirect('/feeds/')
            else :
                messages.info(request,'Username or Password is incorrect..')
    return render(request, 'main/loginPage.html')
    
    
def signUpPage(request):
    if request.user.is_authenticated:
        return redirect ('feeds')
    else :
        form = CreateUserForm()
        message = ""
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Succcess making account for ' + user)
                return redirect('/login/')
    
    response = {'form' : form,}
    return render(request, 'main/signUpPage.html', response)

def logoutPage(request):
    logout(request)
    return redirect('/feeds/')

def feeds(request):
    messagen = Messages.objects.all()
    response = {
        'posts' : messagen,
    }
    return render(request, 'main/feedsPage.html', response)

def myFeeds(request):
    username = request.user.username
    messagen = Messages.objects.filter(username = username)
    response = {
        'posts' : messagen,
    }
    return render(request, 'main/myFeedsPage.html', response)

def addPost(request):
    response = {
        'form' : addPostForm
    }
    return render(request, 'main/addPost.html', response)

def savePost(request):
    if request.method == "POST":
        form = addPostForm(request.POST)
        if form.is_valid():
            if(not request.user.is_authenticated): username = 'x'
            else :username = request.user.username
            post = form.cleaned_data['post']
            messagex = Messages(username = username, message = post)
            messagex.save()
            return redirect('/feeds/')
    return HttpResponseRedirect('/addPost/')




